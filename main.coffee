$(document).ready ->
  $('#home').on 'click', ->
    $('#content').load 'Home.html'
    return
  return

$(document).ready ->
  $('#story').on 'click', ->
    $('#content').load 'Story.html'
    return
  return

$(document).ready ->
  $('#contact').on 'click', ->
    $('#content').load 'Contact.html'
    return
  return

$(document).ready ->
  $('#gallery').on 'click', ->
    $('#content').load 'Gallery.html'
    return
  return
# Navigate once to the initial hash value.
# Updates dynamic content based on the fragment identifier.

navigate = ->
# Get a reference to the "content" div.
  contentDiv = document.getElementById('content')
  # Set the "content" div to contain the current hash value.
  contentDiv.innerHTML = location.hash
  return

navigate()
# Navigate whenever the fragment identifier value changes.
window.addEventListener 'hashchange', ->
  navigate()
  return